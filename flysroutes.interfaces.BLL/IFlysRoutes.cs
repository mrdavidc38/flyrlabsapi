﻿using MethodParameters.IT.flysroutes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flysroutes.interfaces.BLL
{
    public interface IFlysRoutes
    {
        Task<getFlyRoutesOut> listaRutas();
    }
}
