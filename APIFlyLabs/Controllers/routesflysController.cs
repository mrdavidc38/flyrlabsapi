﻿using Entities.IT.flysrutes;
using flysroutes.interfaces.BLL;
using MethodParameters.IT.flysroutes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIFlyLabs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class routesflysController : ControllerBase
    {
      

        private readonly IFlysRoutes _iflyroutes;

        public routesflysController(IFlysRoutes iflyroutes)
        {
            this._iflyroutes = iflyroutes;
        }

        [HttpGet]
        [Route("listaRutas")]
        public async Task<IActionResult> listaRutas()
        {
            var output = new getFlyRoutesOut();
            try
            {
                output = await _iflyroutes.listaRutas();
                return new OkObjectResult(output);
            }
            catch (Exception ex)
            {

                throw new ExecutionEngineException(ex.Message);
            }
            


           
        }
    }
}
