﻿using Entities.IT.flysrutes;
using flysroutes.interfaces.BLL;
using MethodParameters.IT.flysroutes;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flysroutes.BLL
{
    public class FlysRoutes : IFlysRoutes
    {
        private readonly IConfiguration Configuration;

        public FlysRoutes(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public async Task<getFlyRoutesOut> listaRutas()
        {
            var output = new getFlyRoutesOut();
            output.listFlys = new List<RoutesResponse>();
           var listas = new List<RoutesResponse>();
            var urlFlysrlabsIntegration = Configuration["urlFlyrLabs"];
            using (var client = new HttpClient())
            {
                //var json = JsonConvert.SerializeObject(input);
                //var data = new StringContent(json, Encoding.UTF8, "application/json");
                //client.DefaultRequestHeaders.Add("Wolkvox-Token", WolkvoxKey);
                client.BaseAddress = new Uri(urlFlysrlabsIntegration);
                var response = await client.GetAsync("");
                if (response.IsSuccessStatusCode)
                {
                    var resultContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<List<RoutesResponse>>(resultContent);

                    if (result.Any())
                    {
                        foreach (var item in result)
                        {
                            var route = new RoutesResponse();
                            route.ArrivalStation = item.ArrivalStation;
                            route.DepartureStation = item.DepartureStation;
                                route.Price = item.Price;
                            route.FlightCarrier = item.FlightCarrier;
                            route.flightNumber = item.flightNumber;
                            output.listFlys.Add(route);
                        }
                        
                    }
                    //output.listFlys = result.ToList();
                    output.result = Entities.IT.General.Result.Success;
                }
            }

            return output;
        }
    }
}
