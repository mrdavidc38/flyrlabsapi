﻿using Entities.IT.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodParameters.IT.General
{
    public class BaseOut
    {
        public Result result { get; set; }

        public string message { get; set; }
    }
}
