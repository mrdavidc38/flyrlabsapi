﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.IT.General
{
    public enum Result
    {
        Success,
        Error,
        NoRecords,
        RecordAlreadyExists,
        AssociatedRecord,
        DocumentErrorBiometry,
        InactiveUser
    }
}
