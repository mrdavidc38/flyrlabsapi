﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.IT.flysrutes
{
    public class Transport
    {
        public string flightCarrier { get; set; }
        public string flightNumber { get; set; }
    }
}
