﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.IT.flysrutes
{
    public class Journey
    {
        public List<Flight> flights { get; set; }
        public string origin { get; set; }

        public string destination { get; set; }

        public float price { get; set; }

    }
}
